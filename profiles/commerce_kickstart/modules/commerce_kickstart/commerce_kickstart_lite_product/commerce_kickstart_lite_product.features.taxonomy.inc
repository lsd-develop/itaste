<?php
/**
 * @file
 * commerce_kickstart_lite_product.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function commerce_kickstart_lite_product_taxonomy_default_vocabularies() {
  return array(
    'categorie_correlate' => array(
      'name' => 'Categorie correlate',
      'machine_name' => 'categorie_correlate',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '0',
    ),
    'product_category' => array(
      'name' => 'Shop',
      'machine_name' => 'product_category',
      'description' => '',
      'hierarchy' => '0',
      'module' => 'taxonomy',
      'weight' => '0',
      'language' => 'und',
      'i18n_mode' => '0',
    ),
  );
}
