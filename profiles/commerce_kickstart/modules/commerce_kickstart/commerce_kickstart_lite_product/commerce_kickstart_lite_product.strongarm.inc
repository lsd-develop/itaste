<?php
/**
 * @file
 * commerce_kickstart_lite_product.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function commerce_kickstart_lite_product_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_product_display';
  $strongarm->value = 0;
  $export['comment_anonymous_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_product_display';
  $strongarm->value = 0;
  $export['comment_default_mode_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_product_display';
  $strongarm->value = '50';
  $export['comment_default_per_page_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_product_display';
  $strongarm->value = 1;
  $export['comment_form_location_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_product_display';
  $strongarm->value = '1';
  $export['comment_preview_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_product_display';
  $strongarm->value = '1';
  $export['comment_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_product_display';
  $strongarm->value = 0;
  $export['comment_subject_field_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_commerce_product__product';
  $strongarm->value = array(
    'view_modes' => array(
      'line_item' => array(
        'custom_settings' => TRUE,
      ),
      'node_teaser' => array(
        'custom_settings' => TRUE,
      ),
      'node_product_list' => array(
        'custom_settings' => TRUE,
      ),
      'product_in_cart' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'commerce_line_item_display' => array(
        'custom_settings' => FALSE,
      ),
      'node_full' => array(
        'custom_settings' => FALSE,
      ),
      'node_rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'commerce_line_item_token' => array(
        'custom_settings' => FALSE,
      ),
      'node_token' => array(
        'custom_settings' => FALSE,
      ),
      'add_to_cart_confirmation_view' => array(
        'custom_settings' => TRUE,
      ),
      'node_revision' => array(
        'custom_settings' => FALSE,
      ),
      'product_grid' => array(
        'custom_settings' => FALSE,
      ),
      'node_product_grid' => array(
        'custom_settings' => TRUE,
      ),
      'attribute_view' => array(
        'custom_settings' => FALSE,
      ),
      'product_in_cart_block' => array(
        'custom_settings' => TRUE,
      ),
      'node_colorbox' => array(
        'custom_settings' => FALSE,
      ),
      'node_blocco_info_homepage' => array(
        'custom_settings' => FALSE,
      ),
      'node_produttore_in_pagina_prodotto' => array(
        'custom_settings' => FALSE,
      ),
      'node_produttore_in_prodotti_del_produ' => array(
        'custom_settings' => FALSE,
      ),
      'node_scheda_prodotto_in_blog' => array(
        'custom_settings' => FALSE,
      ),
      'node_bundle_product_grid' => array(
        'custom_settings' => TRUE,
      ),
      'commerce_bundle_add_to_cart_form' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'sku' => array(
          'weight' => '0',
        ),
        'title' => array(
          'weight' => '-5',
        ),
        'status' => array(
          'weight' => '4',
        ),
      ),
      'display' => array(
        'sku' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'node_product_list' => array(
            'weight' => '3',
            'visible' => FALSE,
          ),
          'add_to_cart_confirmation_view' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'node_product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'product_in_cart' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'product_in_cart_block' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'node_bundle_product_grid' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
        ),
        'title' => array(
          'default' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'node_product_list' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'node_product_grid' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'product_in_cart_block' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
          'node_bundle_product_grid' => array(
            'weight' => '1',
            'visible' => TRUE,
          ),
        ),
        'status' => array(
          'default' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'node_product_list' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'add_to_cart_confirmation_view' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'node_product_grid' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'product_in_cart' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'product_in_cart_block' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'node_bundle_product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_commerce_product__product'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__product_display';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'product_list' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
      'product_grid' => array(
        'custom_settings' => TRUE,
      ),
      'produttore_in_pagina_prodotto' => array(
        'custom_settings' => FALSE,
      ),
      'colorbox' => array(
        'custom_settings' => FALSE,
      ),
      'blocco_info_homepage' => array(
        'custom_settings' => FALSE,
      ),
      'scheda_prodotto_in_blog' => array(
        'custom_settings' => TRUE,
      ),
      'search_index' => array(
        'custom_settings' => FALSE,
      ),
      'search_result' => array(
        'custom_settings' => FALSE,
      ),
      'produttore_in_prodotti_del_produ' => array(
        'custom_settings' => FALSE,
      ),
      'bundle_product_grid' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'path' => array(
          'weight' => '11',
        ),
      ),
      'display' => array(
        'product:sku' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'product_list' => array(
            'weight' => '30',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
        'product:title' => array(
          'default' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '31',
            'visible' => FALSE,
          ),
        ),
        'product:status' => array(
          'default' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '32',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '28',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_price' => array(
          'default' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'product_list' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
        ),
        'product:field_images' => array(
          'default' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
          'product_list' => array(
            'weight' => '16',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '0',
            'visible' => TRUE,
          ),
        ),
        'product:title_field' => array(
          'product_list' => array(
            'weight' => '29',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '29',
            'visible' => FALSE,
          ),
        ),
        'product:field_colore' => array(
          'product_list' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '34',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '25',
            'visible' => FALSE,
          ),
        ),
        'product:field_cantina' => array(
          'product_list' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '32',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '27',
            'visible' => FALSE,
          ),
        ),
        'product:field_formato' => array(
          'product_list' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '35',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '16',
            'visible' => FALSE,
          ),
        ),
        'product:field_vitigno' => array(
          'product_list' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '33',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '16',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '26',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_items' => array(
          'product_list' => array(
            'weight' => '27',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_unit_quantity' => array(
          'product_list' => array(
            'weight' => '24',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_group_price' => array(
          'product_list' => array(
            'weight' => '23',
            'visible' => FALSE,
          ),
          'default' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'scheda_prodotto_in_blog' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_stock' => array(
          'default' => array(
            'weight' => '16',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
        ),
        'language' => array(
          'product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_extended_product_display';
  $strongarm->value = 1;
  $export['i18n_node_extended_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_options_product_display';
  $strongarm->value = array();
  $export['i18n_node_options_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_sync_node_type_product_display';
  $strongarm->value = array();
  $export['i18n_sync_node_type_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_product_display';
  $strongarm->value = '2';
  $export['language_content_type_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_product_display';
  $strongarm->value = array();
  $export['menu_options_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_product_display';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_product_display';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_product_display';
  $strongarm->value = '0';
  $export['node_preview_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_product_display';
  $strongarm->value = 0;
  $export['node_submitted_product_display'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_pattern';
  $strongarm->value = '[node:content-type:machine-name]/[node:title]';
  $export['pathauto_node_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_punctuation_hyphen';
  $strongarm->value = '1';
  $export['pathauto_punctuation_hyphen'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_pattern';
  $strongarm->value = '[term:vocabulary]/[term:name]';
  $export['pathauto_taxonomy_term_pattern'] = $strongarm;

  return $export;
}
