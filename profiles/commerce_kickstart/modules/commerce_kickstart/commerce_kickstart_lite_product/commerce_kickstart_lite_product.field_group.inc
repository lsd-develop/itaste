<?php
/**
 * @file
 * commerce_kickstart_lite_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function commerce_kickstart_lite_product_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_additional_information|node|product_display|default';
  $field_group->group_name = 'group_additional_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_group';
  $field_group->data = array(
    'label' => 'Curiosità',
    'weight' => '32',
    'children' => array(
      0 => 'field_resi_e_consegna',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Curiosità',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-additional-information field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_additional_information|node|product_display|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_desc_left|node|product_display|default';
  $field_group->group_name = 'group_desc_left';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_description';
  $field_group->data = array(
    'label' => '',
    'weight' => '31',
    'children' => array(
      0 => 'field_location',
      1 => 'product:field_cantina',
      2 => 'product:field_vitigno',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-desc-left field-group-fieldset',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_desc_left|node|product_display|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_desc_right|node|product_display|default';
  $field_group->group_name = 'group_desc_right';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_description';
  $field_group->data = array(
    'label' => '',
    'weight' => '32',
    'children' => array(
      0 => 'field_anno_di_produzione',
      1 => 'product:field_colore',
      2 => 'product:field_formato',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-desc-right field-group-fieldset',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_desc_right|node|product_display|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_info_produttore|node|product_display|default';
  $field_group->group_name = 'group_info_produttore';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_group';
  $field_group->data = array(
    'label' => 'Info Produttore',
    'weight' => '33',
    'children' => array(
      0 => 'field_produttore',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-info-produttore field-group-htab',
        'id' => '',
      ),
    ),
  );
  $export['group_info_produttore|node|product_display|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_description|node|product_display|default';
  $field_group->group_name = 'group_product_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_group';
  $field_group->data = array(
    'label' => 'Descrizione',
    'weight' => '31',
    'children' => array(
      0 => 'body',
      1 => 'group_desc_left',
      2 => 'group_desc_right',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Descrizione',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-product-description field-group-htab',
        'description' => '',
      ),
      'formatter' => 'closed',
    ),
  );
  $export['group_product_description|node|product_display|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_group|node|product_display|default';
  $field_group->group_name = 'group_product_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'product_display';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product Group',
    'weight' => '5',
    'children' => array(
      0 => 'group_product_description',
      1 => 'group_additional_information',
      2 => 'group_info_produttore',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-product-group field-group-htabs',
      ),
    ),
  );
  $export['group_product_group|node|product_display|default'] = $field_group;

  return $export;
}
