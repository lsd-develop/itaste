<?php
/**
 * @file
 * commerce_kickstart_block.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function commerce_kickstart_block_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-payment-methods.
  $menus['menu-payment-methods'] = array(
    'menu_name' => 'menu-payment-methods',
    'title' => 'Payment methods',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Exported menu: secondary-navigation.
  $menus['secondary-navigation'] = array(
    'menu_name' => 'secondary-navigation',
    'title' => 'Secondary navigation',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Payment methods');
  t('Secondary navigation');


  return $menus;
}
