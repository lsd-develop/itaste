<?php
/**
 * @file
 * itaste_product.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function itaste_product_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'ds_views|prodotti_da_categorie_correlate-block-fields|default';
  $ds_fieldsetting->entity_type = 'ds_views';
  $ds_fieldsetting->bundle = 'prodotti_da_categorie_correlate-block-fields';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'field_image' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title_field' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'commerce_price' => array(
      'weight' => '3',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['ds_views|prodotti_da_categorie_correlate-block-fields|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|prodotto_bundle|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'prodotto_bundle';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'lista_dei_prodotti_di_un_bundle' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'links' => array(
      'weight' => '5',
      'label' => 'inline',
      'format' => 'default',
    ),
  );
  $export['node|prodotto_bundle|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|prodotto_bundle|product_list';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'prodotto_bundle';
  $ds_fieldsetting->view_mode = 'product_list';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '1',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
  );
  $export['node|prodotto_bundle|product_list'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|product_display|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'product_display';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'filtra_per_produttore' => array(
      'weight' => '6',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'show_title' => 0,
        'title_wrapper' => '',
        'ctools' => 'a:3:{s:4:"conf";a:2:{s:14:"override_title";i:0;s:19:"override_title_text";s:0:"";}s:4:"type";s:5:"block";s:7:"subtype";s:38:"views-ed64742a6141d255a3efaf8ba9a0d112";}',
        'load_terms' => 0,
      ),
    ),
    'links' => array(
      'weight' => '7',
      'label' => 'inline',
      'format' => 'default',
    ),
  );
  $export['node|product_display|default'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function itaste_product_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'lista_dei_prodotti_di_un_bundle';
  $ds_field->label = 'Lista dei prodotti di un bundle';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
    'commerce_product' => 'commerce_product',
  );
  $ds_field->ui_limit = '';
  $ds_field->properties = array(
    'block' => 'views|list_product_in_bundle-block',
    'block_render' => '3',
  );
  $export['lista_dei_prodotti_di_un_bundle'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function itaste_product_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'comment|comment_node_product_display|default';
  $ds_layout->entity_type = 'comment';
  $ds_layout->bundle = 'comment_node_product_display';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'field_rate_product',
        1 => 'comment_body',
      ),
    ),
    'fields' => array(
      'field_rate_product' => 'header',
      'comment_body' => 'header',
    ),
    'classes' => array(),
    'wrappers' => array(),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => TRUE,
    'layout_link_attribute' => FALSE,
    'layout_link_custom' => '',
  );
  $export['comment|comment_node_product_display|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_product|accessorio_cucina|product_in_cart';
  $ds_layout->entity_type = 'commerce_product';
  $ds_layout->bundle = 'accessorio_cucina';
  $ds_layout->view_mode = 'product_in_cart';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title_field',
      ),
      'left' => array(
        1 => 'field_images',
      ),
      'right' => array(
        2 => 'sku',
      ),
    ),
    'fields' => array(
      'title_field' => 'header',
      'field_images' => 'left',
      'sku' => 'right',
    ),
    'limit' => array(
      'field_images' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['commerce_product|accessorio_cucina|product_in_cart'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_product|food|product_in_cart';
  $ds_layout->entity_type = 'commerce_product';
  $ds_layout->bundle = 'food';
  $ds_layout->view_mode = 'product_in_cart';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'sku',
      ),
      'header' => array(
        1 => 'title_field',
      ),
      'left' => array(
        2 => 'field_images',
      ),
    ),
    'fields' => array(
      'sku' => 'right',
      'title_field' => 'header',
      'field_images' => 'left',
    ),
    'limit' => array(
      'field_images' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['commerce_product|food|product_in_cart'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_product|product|product_in_cart';
  $ds_layout->entity_type = 'commerce_product';
  $ds_layout->bundle = 'product';
  $ds_layout->view_mode = 'product_in_cart';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_images',
      ),
      'right' => array(
        1 => 'title_field',
      ),
    ),
    'fields' => array(
      'field_images' => 'left',
      'title_field' => 'right',
    ),
    'limit' => array(
      'field_images' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['commerce_product|product|product_in_cart'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'commerce_product|vino|product_in_cart';
  $ds_layout->entity_type = 'commerce_product';
  $ds_layout->bundle = 'vino';
  $ds_layout->view_mode = 'product_in_cart';
  $ds_layout->layout = 'ds_2col_stacked_fluid';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_images',
      ),
      'right' => array(
        1 => 'title_field',
        2 => 'field_vitigno',
        3 => 'field_formato',
        4 => 'field_cantina',
        5 => 'field_colore',
      ),
    ),
    'fields' => array(
      'field_images' => 'left',
      'title_field' => 'right',
      'field_vitigno' => 'right',
      'field_formato' => 'right',
      'field_cantina' => 'right',
      'field_colore' => 'right',
    ),
    'limit' => array(
      'field_images' => '1',
    ),
    'classes' => array(),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['commerce_product|vino|product_in_cart'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'ds_views|prodotti_da_categorie_correlate-block-fields|default';
  $ds_layout->entity_type = 'ds_views';
  $ds_layout->bundle = 'prodotti_da_categorie_correlate-block-fields';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title_field',
        2 => 'commerce_price',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title_field' => 'right',
      'commerce_price' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'left' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'hide_page_title' => '0',
    'page_option_title' => '',
  );
  $export['ds_views|prodotti_da_categorie_correlate-block-fields|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|prodotto_bundle|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'prodotto_bundle';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'lista_dei_prodotti_di_un_bundle',
        2 => 'title',
        3 => 'field_product',
        4 => 'product:sku',
        5 => 'group_product_group',
        6 => 'links',
        7 => 'body',
        8 => 'group_product_description',
        9 => 'group_product_list',
      ),
      'left' => array(
        1 => 'field_image',
      ),
    ),
    'fields' => array(
      'lista_dei_prodotti_di_un_bundle' => 'right',
      'field_image' => 'left',
      'title' => 'right',
      'field_product' => 'right',
      'product:sku' => 'right',
      'group_product_group' => 'right',
      'links' => 'right',
      'body' => 'right',
      'group_product_description' => 'right',
      'group_product_list' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|prodotto_bundle|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|prodotto_bundle|product_grid';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'prodotto_bundle';
  $ds_layout->view_mode = 'product_grid';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'field_image',
        1 => 'title_field',
        2 => 'field_product',
      ),
    ),
    'fields' => array(
      'field_image' => 'ds_content',
      'title_field' => 'ds_content',
      'field_product' => 'ds_content',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|prodotto_bundle|product_grid'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|prodotto_bundle|product_list';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'prodotto_bundle';
  $ds_layout->view_mode = 'product_list';
  $ds_layout->layout = 'product_list';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title',
        2 => 'product:commerce_bundle_group_price',
        3 => 'body',
        4 => 'field_product',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title' => 'right',
      'product:commerce_bundle_group_price' => 'right',
      'body' => 'right',
      'field_product' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|prodotto_bundle|product_list'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'right' => array(
        0 => 'field_resi_e_consegna',
        2 => 'title_field',
        3 => 'product:commerce_price',
        4 => 'product:sku',
        5 => 'field_product',
        6 => 'group_product_group',
        7 => 'filtra_per_produttore',
        8 => 'links',
        9 => 'field_produttore',
        10 => 'body',
        11 => 'field_location',
        12 => 'group_product_description',
        13 => 'group_desc_left',
        14 => 'group_desc_right',
        15 => 'group_additional_information',
        16 => 'product:field_cantina',
        17 => 'product:field_vitigno',
        18 => 'group_info_produttore',
        19 => 'product:field_colore',
        20 => 'product:field_formato',
        21 => 'field_anno_di_produzione',
      ),
      'left' => array(
        1 => 'product:field_images',
      ),
    ),
    'fields' => array(
      'field_resi_e_consegna' => 'right',
      'product:field_images' => 'left',
      'title_field' => 'right',
      'product:commerce_price' => 'right',
      'product:sku' => 'right',
      'field_product' => 'right',
      'group_product_group' => 'right',
      'filtra_per_produttore' => 'right',
      'links' => 'right',
      'field_produttore' => 'right',
      'body' => 'right',
      'field_location' => 'right',
      'group_product_description' => 'right',
      'group_desc_left' => 'right',
      'group_desc_right' => 'right',
      'group_additional_information' => 'right',
      'product:field_cantina' => 'right',
      'product:field_vitigno' => 'right',
      'group_info_produttore' => 'right',
      'product:field_colore' => 'right',
      'product:field_formato' => 'right',
      'field_anno_di_produzione' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product_display|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display|form';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display';
  $ds_layout->view_mode = 'form';
  $ds_layout->layout = 'ds_2col_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'header' => array(
        0 => 'title_field',
      ),
      'left' => array(
        1 => 'field_image',
        2 => 'field_produttore',
        3 => 'field_anno_di_produzione',
      ),
      'right' => array(
        4 => 'field_location',
      ),
      'footer' => array(
        5 => 'field_product',
        6 => 'body',
        7 => 'field_resi_e_consegna',
        8 => 'field_product_category',
        9 => 'field_categorie_da_cui_prendere_',
        10 => 'field_correlati_in_cui_inserire_',
        11 => 'path',
      ),
      'hidden' => array(
        12 => '_add_existing_field',
      ),
    ),
    'fields' => array(
      'title_field' => 'header',
      'field_image' => 'left',
      'field_produttore' => 'left',
      'field_anno_di_produzione' => 'left',
      'field_location' => 'right',
      'field_product' => 'footer',
      'body' => 'footer',
      'field_resi_e_consegna' => 'footer',
      'field_product_category' => 'footer',
      'field_categorie_da_cui_prendere_' => 'footer',
      'field_correlati_in_cui_inserire_' => 'footer',
      'path' => 'footer',
      '_add_existing_field' => 'hidden',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product_display|form'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|product_display|product_list';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'product_display';
  $ds_layout->view_mode = 'product_list';
  $ds_layout->layout = 'product_list';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_image',
      ),
      'right' => array(
        1 => 'title_field',
        2 => 'product:commerce_price',
        3 => 'body',
        4 => 'field_product',
      ),
    ),
    'fields' => array(
      'field_image' => 'left',
      'title_field' => 'right',
      'product:commerce_price' => 'right',
      'body' => 'right',
      'field_product' => 'right',
    ),
    'classes' => array(
      'layout_class' => array(
        '' => '',
      ),
    ),
    'wrappers' => array(
      'header' => 'div',
      'left' => 'div',
      'right' => 'div',
      'footer' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|product_display|product_list'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function itaste_product_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'bundle_product_grid';
  $ds_view_mode->label = 'Bundle Product Grid';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['bundle_product_grid'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'product_grid';
  $ds_view_mode->label = 'Product Grid';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['product_grid'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'produttore_in_pagina_prodotto';
  $ds_view_mode->label = 'Produttore in Pagina prodotto';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['produttore_in_pagina_prodotto'] = $ds_view_mode;

  return $export;
}
