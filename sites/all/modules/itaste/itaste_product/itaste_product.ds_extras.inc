<?php
/**
 * @file
 * itaste_product.ds_extras.inc
 */

/**
 * Implements hook_ds_vd_info().
 */
function itaste_product_ds_vd_info() {
  $export = array();

  $ds_vd = new stdClass();
  $ds_vd->api_version = 1;
  $ds_vd->vd = 'prodotti_da_categorie_correlate-block-fields';
  $ds_vd->label = 'Prodotti_da_categorie_correlate: Block (Fields)';
  $export['prodotti_da_categorie_correlate-block-fields'] = $ds_vd;

  return $export;
}
