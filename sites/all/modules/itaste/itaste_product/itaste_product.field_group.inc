<?php
/**
 * @file
 * itaste_product.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function itaste_product_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_description|node|prodotto_bundle|default';
  $field_group->group_name = 'group_product_description';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'prodotto_bundle';
  $field_group->mode = 'default';
  $field_group->parent_name = 'group_product_group';
  $field_group->data = array(
    'label' => 'Descrizione',
    'weight' => '12',
    'children' => array(
      0 => 'body',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-product-description field-group-htab',
        'id' => '',
      ),
    ),
  );
  $export['group_product_description|node|prodotto_bundle|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_product_group|node|prodotto_bundle|default';
  $field_group->group_name = 'group_product_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'prodotto_bundle';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Product group',
    'weight' => '4',
    'children' => array(
      0 => 'group_product_description',
      1 => 'group_product_list',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-product-group field-group-htabs',
      ),
    ),
  );
  $export['group_product_group|node|prodotto_bundle|default'] = $field_group;

  return $export;
}
