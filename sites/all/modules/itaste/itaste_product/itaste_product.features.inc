<?php
/**
 * @file
 * itaste_product.features.inc
 */

/**
 * Implements hook_commerce_product_default_types().
 */
function itaste_product_commerce_product_default_types() {
  $items = array(
    'vino' => array(
      'type' => 'vino',
      'name' => 'Vino',
      'description' => '',
      'help' => '',
      'revision' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_api().
 */
function itaste_product_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds_extras" && $api == "ds_extras") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function itaste_product_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function itaste_product_node_info() {
  $items = array(
    'prodotto_bundle' => array(
      'name' => t('Prodotto bundle'),
      'base' => 'node_content',
      'description' => t('Prodotto che è un aggregato di altri prodotti.'),
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'help' => '',
    ),
  );
  return $items;
}
