<?php
/**
 * @file
 * itaste_product.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function itaste_product_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_prodotto_bundle';
  $strongarm->value = 0;
  $export['comment_anonymous_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_prodotto_bundle';
  $strongarm->value = 1;
  $export['comment_default_mode_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_prodotto_bundle';
  $strongarm->value = '50';
  $export['comment_default_per_page_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_prodotto_bundle';
  $strongarm->value = 1;
  $export['comment_form_location_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_prodotto_bundle';
  $strongarm->value = '1';
  $export['comment_preview_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_prodotto_bundle';
  $strongarm->value = '1';
  $export['comment_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_prodotto_bundle';
  $strongarm->value = 1;
  $export['comment_subject_field_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__prodotto_bundle';
  $strongarm->value = array(
    'view_modes' => array(
      'teaser' => array(
        'custom_settings' => TRUE,
      ),
      'product_list' => array(
        'custom_settings' => TRUE,
      ),
      'full' => array(
        'custom_settings' => FALSE,
      ),
      'rss' => array(
        'custom_settings' => FALSE,
      ),
      'token' => array(
        'custom_settings' => FALSE,
      ),
      'blocco_info_homepage' => array(
        'custom_settings' => FALSE,
      ),
      'product_grid' => array(
        'custom_settings' => TRUE,
      ),
      'produttore_in_pagina_prodotto' => array(
        'custom_settings' => FALSE,
      ),
      'produttore_in_prodotti_del_produ' => array(
        'custom_settings' => FALSE,
      ),
      'scheda_prodotto_in_blog' => array(
        'custom_settings' => FALSE,
      ),
      'revision' => array(
        'custom_settings' => FALSE,
      ),
      'bundle_product_grid' => array(
        'custom_settings' => FALSE,
      ),
    ),
    'extra_fields' => array(
      'form' => array(
        'path' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(
        'product:sku' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
        ),
        'product:title' => array(
          'default' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
        ),
        'product:status' => array(
          'default' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '12',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
        ),
        'product:title_field' => array(
          'default' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '10',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_price' => array(
          'default' => array(
            'weight' => '3',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '4',
            'visible' => FALSE,
          ),
        ),
        'product:field_images' => array(
          'default' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
          'product_grid' => array(
            'weight' => '11',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_stock' => array(
          'default' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '5',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_items' => array(
          'default' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '18',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '21',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_unit_quantity' => array(
          'default' => array(
            'weight' => '22',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '17',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '19',
            'visible' => FALSE,
          ),
        ),
        'product:commerce_bundle_group_price' => array(
          'default' => array(
            'weight' => '20',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '2',
            'visible' => FALSE,
          ),
          'product_list' => array(
            'weight' => '2',
            'visible' => TRUE,
          ),
        ),
        'product:field_colore' => array(
          'default' => array(
            'weight' => '6',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '13',
            'visible' => FALSE,
          ),
        ),
        'product:field_cantina' => array(
          'default' => array(
            'weight' => '8',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '15',
            'visible' => FALSE,
          ),
        ),
        'product:field_formato' => array(
          'default' => array(
            'weight' => '9',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '16',
            'visible' => FALSE,
          ),
        ),
        'product:field_vitigno' => array(
          'default' => array(
            'weight' => '7',
            'visible' => FALSE,
          ),
          'product_grid' => array(
            'weight' => '14',
            'visible' => FALSE,
          ),
        ),
        'language' => array(
          'product_list' => array(
            'weight' => '0',
            'visible' => FALSE,
          ),
        ),
      ),
    ),
  );
  $export['field_bundle_settings_node__prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_extended_prodotto_bundle';
  $strongarm->value = 1;
  $export['i18n_node_extended_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_node_options_prodotto_bundle';
  $strongarm->value = array();
  $export['i18n_node_options_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'i18n_sync_node_type_prodotto_bundle';
  $strongarm->value = array();
  $export['i18n_sync_node_type_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_prodotto_bundle';
  $strongarm->value = '2';
  $export['language_content_type_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_prodotto_bundle';
  $strongarm->value = array();
  $export['menu_options_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_prodotto_bundle';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_prodotto_bundle';
  $strongarm->value = array(
    0 => 'status',
  );
  $export['node_options_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_prodotto_bundle';
  $strongarm->value = '0';
  $export['node_preview_prodotto_bundle'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_prodotto_bundle';
  $strongarm->value = 0;
  $export['node_submitted_prodotto_bundle'] = $strongarm;

  return $export;
}
