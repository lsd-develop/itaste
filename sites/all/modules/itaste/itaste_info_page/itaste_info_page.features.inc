<?php
/**
 * @file
 * itaste_info_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function itaste_info_page_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function itaste_info_page_node_info() {
  $items = array(
    'page' => array(
      'name' => t('Info'),
      'base' => 'node_content',
      'description' => t('Usato per le pagine informative e i blocchetti informativi in home page.'),
      'has_title' => '1',
      'title_label' => t('Titolo'),
      'help' => '',
    ),
  );
  return $items;
}
