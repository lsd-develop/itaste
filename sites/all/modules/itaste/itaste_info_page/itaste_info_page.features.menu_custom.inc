<?php
/**
 * @file
 * itaste_info_page.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function itaste_info_page_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-about-us.
  $menus['menu-about-us'] = array(
    'menu_name' => 'menu-about-us',
    'title' => 'About ',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Exported menu: menu-contattaci.
  $menus['menu-contattaci'] = array(
    'menu_name' => 'menu-contattaci',
    'title' => 'Contattaci',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Exported menu: menu-extra.
  $menus['menu-extra'] = array(
    'menu_name' => 'menu-extra',
    'title' => 'Extra',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '5',
  );
  // Exported menu: menu-help.
  $menus['menu-help'] = array(
    'menu_name' => 'menu-help',
    'title' => 'Help',
    'description' => '',
    'language' => 'und',
    'i18n_mode' => '0',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('About ');
  t('Contattaci');
  t('Extra');
  t('Help');


  return $menus;
}
