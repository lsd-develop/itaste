<?php
/**
 * @file
 * itaste_info_page.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function itaste_info_page_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'about_menu_pages';
  $context->description = '';
  $context->tag = 'info pages';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/1' => 'node/1',
        'node/2' => 'node/2',
        'node/3' => 'node/3',
        'node/6' => 'node/6',
        'node/7' => 'node/7',
        'node/8' => 'node/8',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-about-us' => array(
          'module' => 'menu',
          'delta' => 'menu-about-us',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'menu-menu-help' => array(
          'module' => 'menu',
          'delta' => 'menu-help',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'menu-menu-contattaci' => array(
          'module' => 'menu',
          'delta' => 'menu-contattaci',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'menu-menu-extra' => array(
          'module' => 'menu',
          'delta' => 'menu-extra',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('info pages');
  $export['about_menu_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'contact_menu_pages';
  $context->description = '';
  $context->tag = 'info pages';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/36' => 'node/36',
        'node/60' => 'node/60',
        'node/61' => 'node/61',
        'node/62' => 'node/62',
        'node/63' => 'node/63',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-contattaci' => array(
          'module' => 'menu',
          'delta' => 'menu-contattaci',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'menu-menu-about-us' => array(
          'module' => 'menu',
          'delta' => 'menu-about-us',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'menu-menu-help' => array(
          'module' => 'menu',
          'delta' => 'menu-help',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'menu-menu-extra' => array(
          'module' => 'menu',
          'delta' => 'menu-extra',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('info pages');
  $export['contact_menu_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'extra_menu_pages';
  $context->description = '';
  $context->tag = 'info pages';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/96' => 'node/96',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-extra' => array(
          'module' => 'menu',
          'delta' => 'menu-extra',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'menu-menu-about-us' => array(
          'module' => 'menu',
          'delta' => 'menu-about-us',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'menu-menu-help' => array(
          'module' => 'menu',
          'delta' => 'menu-help',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'menu-menu-contattaci' => array(
          'module' => 'menu',
          'delta' => 'menu-contattaci',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('info pages');
  $export['extra_menu_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'help_menu_pages';
  $context->description = '';
  $context->tag = 'info pages';
  $context->conditions = array(
    'menu' => array(
      'values' => array(
        'node/4' => 'node/4',
        'node/5' => 'node/5',
        'node/64' => 'node/64',
        'node/65' => 'node/65',
        'node/66' => 'node/66',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-help' => array(
          'module' => 'menu',
          'delta' => 'menu-help',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
        'menu-menu-about-us' => array(
          'module' => 'menu',
          'delta' => 'menu-about-us',
          'region' => 'sidebar_first',
          'weight' => '-9',
        ),
        'menu-menu-contattaci' => array(
          'module' => 'menu',
          'delta' => 'menu-contattaci',
          'region' => 'sidebar_first',
          'weight' => '-8',
        ),
        'menu-menu-extra' => array(
          'module' => 'menu',
          'delta' => 'menu-extra',
          'region' => 'sidebar_first',
          'weight' => '-7',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('info pages');
  $export['help_menu_pages'] = $context;

  return $export;
}
