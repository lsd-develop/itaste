<?php
/**
 * @file
 * itaste_cart.features.inc
 */

/**
 * Implements hook_views_api().
 */
function itaste_cart_views_api() {
  return array("version" => "3.0");
}
