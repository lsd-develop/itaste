<?php

/**
 * @file
 * Override of preprocess functions.
 */
function tb_metroz_preprocess_node(&$vars) {
    // process theme style
    $skins = nucleus_get_predefined_param('skins', array('default' => t("Default skin")));
    foreach ($skins as $key => $val) {
        if ($vars['node_url'] == base_path() . 'skins/' . $key && (!isset($_COOKIE['nucleus_skin']) || $_COOKIE['nucleus_skin'] != $key)) {
            setcookie('nucleus_skin', $key, time() + 100000, base_path());
            header('Location: ' . $vars['node_url']);
        }
    }

    $vars['created_date'] = date('d M Y', $vars['created']); // format date

    $vars['page'] = ($vars['type'] == 'page') ? TRUE : FALSE; // display node title

    $vars['comments_count'] = false;
    if (isset($vars['content']['links']['comment'])) {
        if (isset($vars['content']['links']['comment']['#links']['comment-comments'])) {
            $vars['comments_count'] = $vars['content']['links']['comment'];
            foreach ($vars['comments_count']['#links'] as $key => $value) {
                if ($key != 'comment-comments') {
                    unset($vars['comments_count']['#links'][$key]);
                }
            }
            $vars['comments_count']['#prefix'] = '| ';
            unset($vars['content']['links']['comment']['#links']['comment-comments']);
        }
    }

    /* if($vars['type'] == 'product_display'){
      $vars['tb_metroz_first_image'] = false;
      foreach($vars['content'] as $key => $image) {
      if(isset($image['#field_type']) && isset($image['#weight']) && $image['#field_type'] == 'image' && $image['#weight'] <= 0) {
      $vars['tb_metroz_first_image'] = drupal_render($image);
      unset($vars['content'][$key]);
      break;
      }
      }
      } */
}

/**
 * Preprocess field.
 */
function tb_metroz_preprocess_field(&$variables) {
    $element = $variables['element'];
    if ($element['#entity_type'] != 'node' || $element['#field_name'] != 'title_field') {
        return;
    }
    $variables['theme_hook_suggestions'][] = 'field__fences_h2__node';
}

/**
 * Override or insert variables into the page template.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 */
function tb_metroz_preprocess_page(&$vars) {
    if (isset($vars['node']) && $vars['node']->type != 'page' && !in_array('page__node__delete', $vars['theme_hook_suggestions'])) {
        $result = db_select('node_type', NULL, array('fetch' => PDO::FETCH_ASSOC))
                        ->fields('node_type', array('name'))
                        ->condition('type', $vars['node']->type)
                        ->execute()->fetchField();
        $vars['title'] = $result;
    }

    if ($vars['theme_hook_suggestions'][0] == 'page__comment') {
        if (in_array('page__comment__delete', $vars['theme_hook_suggestions'])) {
            $vars['title'] = t("Delete comment");
        } elseif (in_array('page__comment__edit', $vars['theme_hook_suggestions'])) {
            $vars['title'] = t('Edit comment');
        }
    }
}

/**
 * Implements hook_form_alter().
 */
function tb_metroz_form_alter(&$form, &$form_state, $form_id) {
    if (strstr($form_id, 'commerce_cart_add_to_cart_form')) {
        if (isset($form_state['build_info']['args'][0]->data['context']['view'])) {
            if ($form_state['build_info']['args'][0]->data['context']['view']['view_name'] == 'wishlist') {
                return;
            }
        }

        $form['add_to_wishlist']['#weight'] = 49;
    }
    switch ($form_id) {
        case 'views_form_commerce_cart_form_default':
            foreach ($form['edit_delete'] as $row_id => $row) {
                if (isset($form['edit_delete'][$row_id]['#value'])) {
                    $form['edit_delete'][$row_id]['#value'] = 'X';
                }
            }
            break;
    }
    switch ($form_id) {
        case 'views_wishlist':
            foreach ($form['edit_delete'] as $row_id => $row) {
                if (isset($form['edit_delete'][$row_id]['#value'])) {
                    $form['edit_delete'][$row_id]['#value'] = 'X';
                }
            }
            break;
    }
    switch ($form_id) {
        case 'views_form_shopping_cart_block':
            if (isset($form['edit_delete'])) {
                foreach ($form['edit_delete'] as $row_id => $row) {
                    if (isset($form['edit_delete'][$row_id]['#value'])) {
                        $form['edit_delete'][$row_id]['#value'] = 'X';
                    }
                }
            }
            break;
    }
	
   if ($form_id == 'user_login') {
        $form['name']['#prefix'] = '<div id="' . $form_id . '_form">';
        $form['name']['#prefix'] .= '<h1>' . t('Login') . '</h1>';
        $form['pass']['#suffix'] = l(t('Forgot your password?'), 'user/password', array('attributes' => array('class' => array('login-password'), 'title' => t('Get a new password'))));
        $form['actions']['#suffix'] = '</div>';
        if (variable_get('user_register', USER_REGISTER_VISITORS_ADMINISTRATIVE_APPROVAL) != USER_REGISTER_ADMINISTRATORS_ONLY) {
            $form['actions']['#suffix'] .= '<div class="create-account clearfix">';
            $form['actions']['#suffix'] .= "\r<h2>" . t('I don\'t have an account') . "</h2>";
            $form['actions']['#suffix'] .= "\r" . l(t('Create an account'), 'user/register', array('attributes' => array('class' => array('login-register'), 'title' => t('Create a new account'))));
            $form['actions']['#suffix'] .= '</div>';
        }
    }
    if ($form_id == 'user_pass') {
        $form['name']['#prefix'] = '<div id="' . $form_id . '_form">';
        $form['name']['#prefix'] .= '<h1>' . t('Request a new password') . '</h1>';
        $form['actions']['#suffix'] = '<div class="back-to-login clearfix">' . l(t('Back to login'), 'user/login', array('attributes' => array('class' => array('login-account'), 'title' => t('Sign in')))) . '</div>';
        $form['actions']['#suffix'] .= '</div>';
    }
    if ($form_id == 'user_register_form') {
        $form['account']['name']['#prefix'] = '<div id="' . $form_id . '">';
        $form['account']['name']['#prefix'] .= '<h1>' . t('Create Account') . '</h1>';
        $form['actions']['submit']['#suffix'] = '<div class="back-to-login clearfix">' . l(t('Back to login'), 'user/login', array('attributes' => array('class' => array('login-account'), 'title' => t('Sign in')))) . '</div>';
        $form['actions']['submit']['#suffix'] .= '</div>';
    }
}

function tb_metroz_preprocess_comment(&$vars) {
    $vars['created_date'] = date('d F Y', $vars['comment']->created);
}

/*
 * GEt language code instead name
 */

function tb_metroz_links__locale_block(&$vars) {
    foreach ($vars['links'] as $language => $langInfo) {
        $vars['links'][$language]['title'] = $vars['links'][$language]['language']->language;
    }
    $content = theme_links($vars);
    return $content;
}

/**
 * Preprocess variables for the html template.
 */
function tb_metroz_preprocess_html(&$vars) {
    drupal_add_js(path_to_theme() . "/js/views_slideshow.js", 'file');
}

/**
* Implements hook_commerce_line_item_summary_link_info_alter().
* Change link destination/href of checkout link
*/
function tb_metroz_commerce_line_item_summary_link_info_alter(&$links) {
  // Link checkout button to the cart.
  if (isset($links['checkout'])) {
    $links['checkout']['href'] = 'checkout';
  }
}

function tb_metroz_form_element($variables) {
  $element = &$variables['element'];

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';
  
  if((isset($element['#name']) && $element['#name'] == "quantity") || (isset($element['#array_parents']) && $element['#array_parents'][0] == "edit_quantity") && !(isset($element['#attributes']['disabled']))){
	  $prefix = '<div class="minus" data-field="'.$element['#name'].'"> < </div>';
	  $suffix = '<div class="plus" data-field="'.$element['#name'].'"> > </div>';
  }

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  if (!empty($element['#description'])) {
    $output .= '<div class="description">' . $element['#description'] . "</div>\n";
  }

  $output .= "</div>\n";

  return $output;
}

