<?php

/**
 * @file
 * Display Suite Three column - smallest on the right configuration.
 */

function ds_three_column__smallest_on_the_right() {
  return array(
    'label' => t('Three column - smallest on the right'),
    'regions' => array(
      'top' => t('Top'),
      'left' => t('Left'),
      'center' => t('Center'),
      'right' => t('Right'),
      'bottom' => t('Bottom'),
    ),
    // Uncomment if you want to include a CSS file for this layout (three_column__smallest_on_the_right.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (three_column__smallest_on_the_right.png)
    // 'image' => TRUE,
  );
}
