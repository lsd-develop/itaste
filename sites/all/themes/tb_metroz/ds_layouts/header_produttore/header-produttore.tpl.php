<?php
/**
 * @file
 * Display Suite header produttore template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $description: Rendered content for the "Description" region.
 * - $description_classes: String of classes that can be used to style the "Description" region.
 *
 * - $logo: Rendered content for the "Logo" region.
 * - $logo_classes: String of classes that can be used to style the "Logo" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> clearfix">

  <!-- Needed to activate contextual links -->
  <?php if (isset($title_suffix['contextual_links'])): ?>
    <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

    <<?php print $description_wrapper; ?> class="ds-description<?php print $description_classes; ?>">
      <?php print $description; ?>
    </<?php print $description_wrapper; ?>>

    <<?php print $logo_wrapper; ?> class="ds-logo<?php print $logo_classes; ?>">
      <?php print $logo; ?>
    </<?php print $logo_wrapper; ?>>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
