<?php

/**
 * @file
 * Display Suite header produttore configuration.
 */

function ds_header_produttore() {
  return array(
    'label' => t('header produttore'),
    'regions' => array(
      'description' => t('Description'),
      'logo' => t('Logo'),
    ),
    // Uncomment if you want to include a CSS file for this layout (header_produttore.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (header_produttore.png)
    // 'image' => TRUE,
  );
}
