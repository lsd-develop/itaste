<?php
/**
 * @file
 * Display Suite Product List template.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 * - $layout_wrapper: wrapper surrounding the layout.
 *
 * Regions:
 *
 * - $header: Rendered content for the "header" region.
 * - $header_classes: String of classes that can be used to style the "header" region.
 *
 * - $left: Rendered content for the "left" region.
 * - $left_classes: String of classes that can be used to style the "left" region.
 *
 * - $right: Rendered content for the "right" region.
 * - $right_classes: String of classes that can be used to style the "right" region.
 *
 * - $footer: Rendered content for the "footer" region.
 * - $footer_classes: String of classes that can be used to style the "footer" region.
 */
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="<?php print $classes;?> clearfix">

<!-- Needed to activate contextual links -->
<?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
<?php endif; ?>

<?php if($header) : ?>
  <div class="grid grid-17 ">
      <<?php print $header_wrapper; ?> class="ds-header<?php print $header_classes; ?>">
      <?php print $header; ?>
      </<?php print $header_wrapper; ?>>
  </div>
<?php endif ?>

<div class="grid grid-6">
    <<?php print $left_wrapper; ?> class="ds-left<?php print $left_classes; ?>">
    <?php print $left; ?>
    </<?php print $left_wrapper; ?>>
</div>

<div class="grid grid-10">
    <<?php print $right_wrapper; ?> class="ds-right<?php print $right_classes; ?>">
    <?php print $right; ?>
    </<?php print $right_wrapper; ?>>
</div>

<?php if($footer) : ?>
  <div class="grid grid-17">
      <<?php print $footer_wrapper; ?> class="ds-footer<?php print $footer_classes; ?>">
      <?php print $footer; ?>
      </<?php print $footer_wrapper; ?>>
  </div>
<?php endif ?>

</<?php print $layout_wrapper ?>>

<!-- Needed to activate display suite support on forms -->
<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>
