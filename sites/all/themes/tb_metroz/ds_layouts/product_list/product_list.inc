<?php

/**
 * @file
 * Display Suite Product List configuration.
 */

function ds_product_list() {
  return array(
    'label' => t('Product List'),
    'regions' => array(
      'header' => t('header'),
      'left' => t('left'),
      'right' => t('right'),
      'footer' => t('footer'),
    ),
    // Uncomment if you want to include a CSS file for this layout (product_list.css)
    'css' => TRUE,
    // Uncomment if you want to include a preview for this layout (product_list.png)
    'image' => TRUE,
  );
}
