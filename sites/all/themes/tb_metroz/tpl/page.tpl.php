<?php
/**
 * @fil * Default theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['help']: Dynamic help text, mostly for admin pages.
 * - $page['highlighted']: Items for the highlighted content region.
 * - $page['slideshow']: Items for the slideshow region.
 * - $page['content']: The main content of the current page.
 * - $page['sidebar_first']: Items for the first sidebar.
 * - $page['sidebar_second']: Items for the second sidebar.
 * - $page['header']: Items for the header region.
 * - $page['footer']: Items for the footer region.
 * - $panel_first: Items for the regions in panel_first.
 * - $panel_second: Items for the regions in panel_second.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see nucleus_preprocess_page()
 */
?>

<!-- HEADER -->
<div id="header-wrapper" class="wrapper">

    <div id="pulldown-wrapper" class="wrapper">
        <div class="container display-none <?php print $grid; ?>">
            <?php if ($pulldown_left = render($page['pulldown_left'])) : ?>
                <div class="grid grid-16 clearfix padding-top-30 padding-bottom-30 ">
                    <?php print $pulldown_left; ?>
                </div>
            <?php endif; ?>

            <?php if ($pulldown_right = render($page['pulldown_right'])): ?>
                <div class="grid grid-8 clearfix padding-top-30 padding-bottom-30 ">
                    <?php print $pulldown_right; ?>
                </div>
                <div class="clearfix"></div>
            <?php endif; ?>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>



    <div id="topbar-wrapper" class="wrapper position-relative">
        <div class="pulldown-button-wrapper">
            <a href="" id="header-pull"><span>pull me</span></a>
        </div>
        <div class="container <?php print $grid; ?>">
            <?php if ($topbar_left = render($page['topbar_left'])) : ?>
                <div class="grid grid-7 clearfix text-uppercase">
                    <?php print $topbar_left; ?>
                </div>
            <?php endif; ?>
            <?php if ($topbar_center = render($page['topbar_center'])) : ?>
                <div class="grid grid-10 clearfix">
                    <?php print $topbar_center; ?>
                </div>
            <?php endif; ?>

            
            <?php if ($topbar_right = render($page['topbar_right'])) : ?>
                <div class="grid grid-7 clearfix text-uppercase">
                    <?php print $topbar_right; ?>
                </div>
            <?php endif; ?>

            
<!--
            <div class="grid grid-6 clearfix ">
                <div class="float-right topbar-user-menu">
                    <?php if (user_is_logged_in()) { ?>
                    <?php
                        /** <!--  <span class="display-inline padding-right-10 text-uppercase">
                          <a class="text-black" href="<?php echo $front_page; ?>/user/<?php echo($GLOBALS['user']->uid); ?>/wishlist"><i class="fa fa-heart-o fa-fw"></i> <?php  print t('wishlist'); ?></a>
                      </span> */
                      ?>
                      <span class="display-inline text-uppercase">
                        <a class="text-black" href="<?php echo $front_page; ?>/user/"><i class="fa fa-user fa-fw"></i> <?php print t('my account'); ?></a>
                    </span>
                    <span class="display-inline text-uppercase">
                        <a class="text-black" href="<?php echo $front_page; ?>/user/logout"><i class="fa fa-sign-out fa-fw"></i> <?php print t('logout'); ?></a>
                    </span>
                    <?php
                } else {
                    ?>
                    <span class="display-inline padding-right-10 text-uppercase">
                        <a class="text-black" href="<?php echo $front_page; ?>/user/"><i class="fa fa-sign-in fa-fw"></i> <?php print t('login'); ?></a>
                    </span>
                    <span class="display-inline text-uppercase">
                        <a class="text-black" href="<?php echo $front_page; ?>/user/register"><i class="fa fa-user fa-fw"></i> <?php print t('register'); ?></a>
                    </span>
                    <?php } ?>
                </div>
            </div>
        -->
    </div>
</div>

<div class="container <?php print $grid; ?>">
    <div class="grid-inner clearfix">
        <div id="header" class="grid grid-24">

            <?php if ($logo): ?>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" id="logo" class="grid grid-5">
                    <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                </a>
            <?php endif; ?>


            <?php if ($menu_bar = render($page['menu_bar'])): ?>
                <div id="menu-bar-wrapper" class="wrapper grid grid-14">
                    <?php print $menu_bar; ?>
                </div>
            <?php endif; ?>
            <?php if ($region = render($page['cart'])): ?>
                <div id="cart-wrapper" class="grid grid-5">
                    <?php print $region; ?>
                </div>
            <?php endif; ?>
                           

                            </div>
                        </div>

                    </div>
                </div>
                <!-- /#HEADER -->


                <div id="page" class="page-default <?php print isset($page['nucleus_skin_classes']) ? $page['nucleus_skin_classes'] : ""; ?>">
                    <div id="preslideshow_wrapper" class="wrapper">  
                        <div class="container <?php print $grid; ?>">  
                            <?php if ($preslideshow_left = render($page['preslideshow_left'])): ?>  
                                <div class=" grid grid-12 clearfix">
                                    <div id="preslideshow_left" >
                                        <?php print $preslideshow_left; ?>  
                                    </div>  
                                </div>   
                            <?php endif; ?>
                            <?php if ($preslideshow_right = render($page['preslideshow_right'])): ?>  
                                <div class=" grid grid-12 clearfix">
                                    <div id="preslideshow_right" > 
                                        <?php print $preslideshow_right; ?>  
                                    </div>  
                                </div>  
                            </div> 
                        </div> 

                    <?php endif; ?>
                    <?php if ($slideshow = render($page['slideshow'])): ?>
                        <!-- SLIDESHOW -->
                        <div id="slideshow-wrapper" class="wrapper">
                            <div class="container <?php print $grid; ?>">
                                <div class="grid-inner clearfix">
                                    <?php print $slideshow; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /#SLIDESHOW -->
                    <?php endif; ?>

                    <?php if ($is_front && ($messages || $page['help'])): ?>
                        <!-- HELP & MESSAGES -->
                        <div id="system-messages-wrapper" class="wrapper container-16">
                            <div class="container <?php print $grid; ?>">
                                <div class="grid-inner clearfix">
                                    <?php print $messages . render($page['help']); ?>
                                </div>
                            </div>
                        </div>
                        <!-- /#HELP & MESSAGES -->
                    <?php endif; ?>

                    <?php if ($mass_top = render($page['mass_top'])): ?>
                        <div id="mass-top-wrapper" class="wrapper">
                            <div class="container <?php print $grid; ?>">
                                <div class="grid-inner clearfix">
                                    <div class="grid-inner2">
                                        <?php print $mass_top; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($panel_first): ?>
                        <!-- Panel First -->
                        <div id="panel-first-wrapper" class="wrapper panel-first">
                            <div class="container <?php print $grid; ?> clearfix">
                                <div class="grid-inner">
                                    <?php print $panel_first; ?>
                                </div>
                            </div>
                        </div>
                        <!-- /#Panel First -->

                    <?php endif; ?>
                    <div id="main-wrapper" class="wrapper">

                        <?php if (!$is_front && ($messages || $page['help'])): ?>
                            <!-- HELP & MESSAGES -->
                            <div id="system-messages-wrapper" class="wrapper container-16">
                                <div class="container <?php print $grid; ?>">
                                    <div class="grid-inner clearfix">
                                        <?php print $messages . render($page['help']); ?>
                                    </div>
                                </div>
                            </div>
                            <!-- /#HELP & MESSAGES -->
                        <?php endif; ?>
                        <div class="container <?php print $grid; ?> clearfix">
                      <!--  <?php if ($breadcrumb && !$is_front): ?>
                              <div id="breadcrumb-wrapper" class="wrapper clearfix">
                                    <div class="container clearfix">
                                        <div class="grid-inner">
                <?php print $breadcrumb; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>-->
                            <?php print render($title_prefix); ?>
                            <?php if ($title): ?>
                               <div id="main-title-outer">
                                <div class="container clearfix">
                                    <h1 id="page-title"><?php print $title; ?></h1>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php print render($title_suffix); ?>
                        <div class="<?php print nucleus_group_class("content, sidebar_first"); ?>">
                            <?php if ($content_width) : ?>
                                <!-- MAIN CONTENT -->
                                <?php if (($sidebar_first = render($page['sidebar_first'])) && $sidebar_first_width) : ?>
                                    <!-- SIDEBAR FIRST -->
                                    <div id="sidebar-first-wrapper" class="sidebar tb-main-box <?php print $sidebar_first_width; ?> grid-last">
                                        <div class="grid-inner clearfix">
                                            <?php print $sidebar_first; ?>
                                        </div>
                                    </div>
                                    <!-- /#SIDEBAR FIRST -->
                                <?php endif; ?>
                                <div id="main-content" class="<?php print $content_width; ?> section">
                                    <div class="grid-inner clearfix">

                                        <?php if ($tabs = render($tabs)): ?>
                                            <div class="tabs"><?php print $tabs; ?></div>
                                        <?php endif; ?>

                                        <?php if ($highlighted = render($page['highlighted'])): ?>
                                            <?php print $highlighted; ?>
                                        <?php endif; ?>



                                        <?php if ($action_links = render($action_links)): ?>
                                            <ul class="action-links"><?php print $action_links; ?></ul>
                                        <?php endif; ?>

                                        <?php if ($content = render($page['content'])): ?>
                                            <?php print $content; ?>
                                        <?php endif; ?>

                                        <?php print $feed_icons; ?>

                                        <?php if ($panel_second): ?>
                                            <div id="panel-second-wrapper" class="wrapper panel-second">
                                                <div class="container <?php print $grid; ?> clearfix">
                                                    <div class="grid-inner">
                                                        <div class="grid-inner2">
                                                            <?php print $panel_second; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if ($panel_third): ?>
                                            <div id="panel-third-wrapper" class="wrapper panel-third">
                                                <div class="container <?php print $grid; ?> clearfix">
                                                    <div class="grid-inner">
                                                        <div class="grid-inner2">
                                                            <?php print $panel_third; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php if ($mass_bottom = render($page['mass_bottom'])): ?>    </div>

                                            <div id="mass-bottom-wrapper" class="wrapper">
                                                <div class="container <?php print $grid; ?>">
                                                    <div class="grid-inner clearfix">
                                                        <?php print $mass_bottom; ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endif; ?>


                                        <div class="container grid grid-24 wrapper"> 

                                           <?php if ($bottom_left = render($page['bottom_left'])): ?>   
                                              <div id="bottom-left-wrapper" >
                                                  <div class=" grid grid-12">
                                                      <div class="grid-inner clearfix">
                                                          <?php print $bottom_left; ?>
                                                      </div>
                                                  </div>
                                              </div>
                                          <?php endif; ?>

                                          <?php if ($bottom_right = render($page['bottom_right'])): ?>
                                              <div id="bottom-right-wrapper" >
                                                  <div class=" grid grid-12">
                                                      <div class="grid-inner clearfix">
                                                          <?php print $bottom_right; ?>
                                                      </div>
                                                  </div>
                                              </div>
                                          <?php endif; ?>
                                      </div>


                                      <?php if ($back_to_top_display): ?>
                                        <a title="<?php print t('Back to Top'); ?>" class="btn-btt" href="#page">â–² <?php print t('Top'); ?></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <!-- /#MAIN CONTENT -->
                        <?php endif; ?>

                    </div>

                    <?php if (($sidebar_second = render($page['sidebar_second'])) && $sidebar_second_width) : ?>
                        <!-- SIDEBAR SECOND -->
                        <div id="sidebar-first-wrapper" class="sidebar tb-main-box <?php print $sidebar_second_width; ?> grid-last">
                            <div class="grid-inner clearfix">
                                <?php print $sidebar_second; ?>
                            </div>
                        </div>
                        <!-- /#SIDEBAR SECOND -->
                    <?php endif; ?>
                </div>
            </div>




            <?php if ($region = render($page['popover_left'])): ?>
                <div id="popover-left-wrapper" class="special-menu-content" style="display:none" >
                    <?php print $region; ?>
                </div>
            <?php endif; ?>

            <?php if ($region = render($page['popover_right'])): ?>
                <div id="popover-right-wrapper" class="special-menu-content"  style="display:none" >
                    <?php print $region; ?>
                </div>
            <?php endif; ?>



            <?php if ($panel_prefooter): ?>
                <!-- PANEL pre footer -->
                <div id="panel-prefooter-wrapper" class="wrapper panel panel-prefooter">
                    <div class="container <?php print $panel_prefooter_cols; ?> <?php print $grid; ?> clearfix">
                        <?php print $panel_prefooter; ?>
                    </div>
                </div>
                <!-- /#PANEL footer -->
            <?php endif; ?>

            <?php if ($panel_footer): ?>
                <!-- PANEL footer -->
                <div id="panel-footer-wrapper" class="wrapper panel panel-footer">
                    <div class="container <?php print $panel_footer_cols; ?> <?php print $grid; ?> clearfix">

                        <?php print $panel_footer; ?>
                    </div>
                </div>
                <!-- /#PANEL footer -->
            <?php endif; ?>

            <!-- FOOTER -->
            <div id="footer-wrapper" class="wrapper clearfix">
                <div class="container <?php print $grid; ?>">
                    <?php if ($footer_first = render($page['footer_first'])): ?>

                        <div class=" grid grid-24 clearfix">
                            <div id="footer-first" >
                                <?php print $footer_first; ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if ($footer_second = render($page['footer_second'])): ?>
                  <!--  <div class=" grid grid-12 clearfix">
                        <div id="footer-second" class="float-right">
                            <?php print $footer_second; ?>
                        </div>
                    </div>
                -->
                <!-- /#FOOTER -->
            <?php endif; ?>
        </div>
    </div>
    <?php if(!preg_match("/(cart|checkout)/i", current_path())): ?>
        <div id="tools">

    <!--<div id="search-tool">
        <?php //if ($region = render($page['search'])): ?>
            <div id="search-wrapper" class="special-menu-content"  style="display:none">
                <?php //print $region; ?>
            </div>
            <?php //endif; ?></div>-->
            <div id="info-tool" >
                <div class="tool-content">
                    <?php if ($info = render($page['info'])): ?>
                        <div id="info-wrapper" class="special-menu-content" style="display:none">
                            <?php print $info;?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <div id="fav-tool" > 
                <div class="tool-content">
                    <?php if ($wish = render($page['wishlist'])): ?>
                        <div id="wish-wrapper" class="special-menu-content" style="display:none">
                            <?php print $wish; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
