(function ($) {
    Drupal.TBResponsive = Drupal.TBResponsive || {};
    Drupal.TBResponsive.supportedScreens = [0.5, 479.5, 719.5, 959.5, 1049.5];
    Drupal.TBResponsive.oldWindowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    Drupal.TBResponsive.IE8 = $.browser.msie && parseInt($.browser.version, 10) === 8;
    Drupal.TBResponsive.toolbar = false;
    Drupal.TBResponsive.PopularSlideshowSize = false;
    Drupal.TBResponsive.isFresh = true;
  
    Drupal.TBResponsive.updateResponsiveMenu = function(){
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
        if(windowWidth < Drupal.TBResponsive.supportedScreens[3]){
            $('#menu-bar-wrapper').hide();
            $('.responsive-menu-button').show();
        }
        else{
            $('.responsive-menu-button').hide();
            $('#menu-bar-wrapper').show().attr('style','');
        }
    }

    Drupal.TBResponsive.initResponsiveMenu = function(){
        Drupal.TBResponsive.updateResponsiveMenu();
        $('.tb-main-menu-button').bind('click',function(e){
            var target = $('#menu-bar-wrapper');
            if(target.css('display') == 'none') {
                target.css({
                    display: 'block'
                });          
            }
            else {
                target.css({
                    display: 'none'
                });
            }
        });
    }
  
    Drupal.TBResponsive.updateSpecialMenu = function(){
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
        if(windowWidth < Drupal.TBResponsive.supportedScreens[3]){
            $('#header .tb-main-menu-button').data('data','#menu-bar-wrapper');
        }else{
            $('#header .tb-main-menu-button').data('data','');
        }
    
        $('#header .special-menu-content').hide();
    }
  
    Drupal.TBResponsive.initQuicktabsMenu = function(){
        var resMenu = $('<div class="quicktabs-responsive-menu" style="display: none"></div>');      
        var title = $('<span class="title"></span>');
        var button = $('<span class="button">Toogle</span>');
        resMenu.append(title);
        resMenu.append(button);
        var menu = $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs');
        menu.before(resMenu);
        button.bind('click', function(){
            var menu = $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs');
            if(menu.css('display') == 'none') menu.show();
            else menu.hide();
        });
        $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs a').bind('click', function(){
            var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
            title.text($(this).text());
            if(windowWidth < Drupal.TBResponsive.supportedScreens[2]){
                $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs').hide();
            }      
        });
        $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs li.active a').trigger('click');
    
        Drupal.TBResponsive.updateQuicktabsMenu();
    }
  
    Drupal.TBResponsive.updateQuicktabsMenu = function(){
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    
        if(windowWidth < Drupal.TBResponsive.supportedScreens[2]){
            $('#main-wrapper .quicktabs-responsive-menu').show();
            $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs').hide();
        }else{
            $('#main-wrapper .quicktabs-responsive-menu').hide();
            $('#main-wrapper .quicktabs-wrapper .quicktabs-tabs').show();
        }
    }    
  
    Drupal.TBResponsive.updatePopularSlideshowHeight = function(){
        var slideshow = $('#panel-second-wrapper .panel-second-1 .views-slideshow-cycle-main-frame');
        if(slideshow.length == 0) return;
        var imgs = slideshow.find('img');
        if(imgs.length) {
            if(!Drupal.TBResponsive.PopularSlideshowSize) {
                var img = new Image();
                img.src = $(imgs[0]).attr('src');
                Drupal.TBResponsive.getImageSize(img);
                setTimeout(Drupal.TBResponsive.updatePopularSlideshowHeight, 200)
                return;
            }
        }  
    
        slideshow.cycle('destroy');
        $('#panel-second-wrapper .panel-second-1 .views-slideshow-cycle-main-frame').cycle('destroy');
        var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();    
        var panelWidth = $('#panel-second-wrapper').width();
        var width = (windowWidth < Drupal.TBResponsive.supportedScreens[2]) ? panelWidth : 
        (panelWidth - $('#panel-second-wrapper .panel-second-2').width() - 30);
        var height = width * Drupal.TBResponsive.PopularSlideshowSize.height / Drupal.TBResponsive.PopularSlideshowSize.width;
        $('#panel-second-wrapper .panel-second-1 .views-slideshow-cycle-main-frame-row img, #panel-second-wrapper .panel-second-1 .views-slideshow-cycle-main-frame-row, #panel-second-wrapper .panel-second-1 .views-slideshow-cycle-main-frame').height(height).width(width);
        slideshow.find('.views-slideshow-cycle-main-frame-row .views-fields-wrapper').width(width - 40);
        slideshow.cycle();
    }
  
    Drupal.TBResponsive.getImageSize = function(img) {
        if(img.height == 0) {
            setTimeout(function() {
                Drupal.TBResponsive.getImageSize(img);
            }, 200);
            return;
        }
        if(!Drupal.TBResponsive.PopularSlideshowSize) {
            Drupal.TBResponsive.PopularSlideshowSize = {
                height: img.height, 
                width: img.width
                };
        }
    }
  
    Drupal.TBResponsive.setSidebarAccordionHeight = function(){
        $('#sidebar-first-wrapper .ui-accordion-content').css('height','');
    }
  
    Drupal.TBResponsive.fixTopBar = function(){
        var height = Drupal.TBResponsive.toolbar ? (Drupal.TBResponsive.toolbar.height() - (Drupal.TBResponsive.IE8 ? 10 : 0)) : 0;
        // when administration toolbar is displayed
        $('body').css({
            'padding-top': height
        });
        $('#header-wrapper').css({
            'top': height
        });
    }
  
    Drupal.behaviors.actionTBResponsive = {
        attach: function (context) {
            if(!Drupal.TBResponsive.isFresh) return; // Fix repeat binding issues for a product display with multiple product references.
      
            $(window).load(function(){        
                Drupal.TBResponsive.updateResponsiveMenu();
                Drupal.TBResponsive.updateSpecialMenu();
                Drupal.TBResponsive.initQuicktabsMenu();
                Drupal.TBResponsive.updatePopularSlideshowHeight();
                Drupal.TBResponsive.toolbar = $('#toolbar').length ? $("#toolbar") : false;
                Drupal.TBResponsive.fixTopBar();
        
                $(window).resize(function(){     
                    Drupal.TBResponsive.fixTopBar();
                    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
                    if(windowWidth != Drupal.TBResponsive.oldWindowWidth){
                        Drupal.TBResponsive.oldWindowWidth = windowWidth;            
                        Drupal.TBResponsive.updateResponsiveMenu();                 
                        Drupal.TBResponsive.updateSpecialMenu();  
                        Drupal.TBResponsive.updateQuicktabsMenu();            
                        Drupal.TBResponsive.updatePopularSlideshowHeight();
                        Drupal.TBResponsive.setSidebarAccordionHeight();
                        Drupal.TBMetroz.btnToTop();
                    }
                });
            });   
      
            Drupal.TBResponsive.isFresh = false;
        }
    };
})(jQuery);
