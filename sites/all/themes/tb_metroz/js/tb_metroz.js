// VERSION: 2.3 LAST UPDATE: 11.07.2013
/*
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 *
 * Made by Wilq32, wilq32@gmail.com, Wroclaw, Poland, 01.2009
 * Website: http://code.google.com/p/jqueryrotate/
 */
(function(k){for(var d,f,l=document.getElementsByTagName("head")[0].style,h=["transformProperty","WebkitTransform","OTransform","msTransform","MozTransform"],g=0;g<h.length;g++)void 0!==l[h[g]]&&(d=h[g]);d&&(f=d.replace(/[tT]ransform/,"TransformOrigin"),"T"==f[0]&&(f[0]="t"));eval('IE = "v"=="\v"');jQuery.fn.extend({rotate:function(a){if(0!==this.length&&"undefined"!=typeof a){"number"==typeof a&&(a={angle:a});for(var b=[],c=0,d=this.length;c<d;c++){var e=this.get(c);if(e.Wilq32&&e.Wilq32.PhotoEffect)e.Wilq32.PhotoEffect._handleRotation(a);
else{var f=k.extend(!0,{},a),e=(new Wilq32.PhotoEffect(e,f))._rootObj;b.push(k(e))}}return b}},getRotateAngle:function(){for(var a=[],b=0,c=this.length;b<c;b++){var d=this.get(b);d.Wilq32&&d.Wilq32.PhotoEffect&&(a[b]=d.Wilq32.PhotoEffect._angle)}return a},stopRotate:function(){for(var a=0,b=this.length;a<b;a++){var c=this.get(a);c.Wilq32&&c.Wilq32.PhotoEffect&&clearTimeout(c.Wilq32.PhotoEffect._timer)}}});Wilq32=window.Wilq32||{};Wilq32.PhotoEffect=function(){return d?function(a,b){a.Wilq32={PhotoEffect:this};
this._img=this._rootObj=this._eventObj=a;this._handleRotation(b)}:function(a,b){this._img=a;this._onLoadDelegate=[b];this._rootObj=document.createElement("span");this._rootObj.style.display="inline-block";this._rootObj.Wilq32={PhotoEffect:this};a.parentNode.insertBefore(this._rootObj,a);if(a.complete)this._Loader();else{var c=this;jQuery(this._img).bind("load",function(){c._Loader()})}}}();Wilq32.PhotoEffect.prototype={_setupParameters:function(a){this._parameters=this._parameters||{};"number"!==
typeof this._angle&&(this._angle=0);"number"===typeof a.angle&&(this._angle=a.angle);this._parameters.animateTo="number"===typeof a.animateTo?a.animateTo:this._angle;this._parameters.step=a.step||this._parameters.step||null;this._parameters.easing=a.easing||this._parameters.easing||this._defaultEasing;this._parameters.duration=a.duration||this._parameters.duration||1E3;this._parameters.callback=a.callback||this._parameters.callback||this._emptyFunction;this._parameters.center=a.center||this._parameters.center||
["50%","50%"];this._rotationCenterX="string"==typeof this._parameters.center[0]?parseInt(this._parameters.center[0],10)/100*this._imgWidth*this._aspectW:this._parameters.center[0];this._rotationCenterY="string"==typeof this._parameters.center[1]?parseInt(this._parameters.center[1],10)/100*this._imgHeight*this._aspectH:this._parameters.center[1];a.bind&&a.bind!=this._parameters.bind&&this._BindEvents(a.bind)},_emptyFunction:function(){},_defaultEasing:function(a,b,c,d,e){return-d*((b=b/e-1)*b*b*b-
1)+c},_handleRotation:function(a,b){d||this._img.complete||b?(this._setupParameters(a),this._angle==this._parameters.animateTo?this._rotate(this._angle):this._animateStart()):this._onLoadDelegate.push(a)},_BindEvents:function(a){if(a&&this._eventObj){if(this._parameters.bind){var b=this._parameters.bind,c;for(c in b)b.hasOwnProperty(c)&&jQuery(this._eventObj).unbind(c,b[c])}this._parameters.bind=a;for(c in a)a.hasOwnProperty(c)&&jQuery(this._eventObj).bind(c,a[c])}},_Loader:function(){return IE?function(){var a=
this._img.width,b=this._img.height;this._imgWidth=a;this._imgHeight=b;this._img.parentNode.removeChild(this._img);this._vimage=this.createVMLNode("image");this._vimage.src=this._img.src;this._vimage.style.height=b+"px";this._vimage.style.width=a+"px";this._vimage.style.position="absolute";this._vimage.style.top="0px";this._vimage.style.left="0px";this._aspectW=this._aspectH=1;this._container=this.createVMLNode("group");this._container.style.width=a;this._container.style.height=b;this._container.style.position=
"absolute";this._container.style.top="0px";this._container.style.left="0px";this._container.setAttribute("coordsize",a-1+","+(b-1));this._container.appendChild(this._vimage);this._rootObj.appendChild(this._container);this._rootObj.style.position="relative";this._rootObj.style.width=a+"px";this._rootObj.style.height=b+"px";this._rootObj.setAttribute("id",this._img.getAttribute("id"));this._rootObj.className=this._img.className;for(this._eventObj=this._rootObj;a=this._onLoadDelegate.shift();)this._handleRotation(a,
!0)}:function(){this._rootObj.setAttribute("id",this._img.getAttribute("id"));this._rootObj.className=this._img.className;this._imgWidth=this._img.naturalWidth;this._imgHeight=this._img.naturalHeight;var a=Math.sqrt(this._imgHeight*this._imgHeight+this._imgWidth*this._imgWidth);this._width=3*a;this._height=3*a;this._aspectW=this._img.offsetWidth/this._img.naturalWidth;this._aspectH=this._img.offsetHeight/this._img.naturalHeight;this._img.parentNode.removeChild(this._img);this._canvas=document.createElement("canvas");
this._canvas.setAttribute("width",this._width);this._canvas.style.position="relative";this._canvas.style.left=-this._img.height*this._aspectW+"px";this._canvas.style.top=-this._img.width*this._aspectH+"px";this._canvas.Wilq32=this._rootObj.Wilq32;this._rootObj.appendChild(this._canvas);this._rootObj.style.width=this._img.width*this._aspectW+"px";this._rootObj.style.height=this._img.height*this._aspectH+"px";this._eventObj=this._canvas;for(this._cnv=this._canvas.getContext("2d");a=this._onLoadDelegate.shift();)this._handleRotation(a,
!0)}}(),_animateStart:function(){this._timer&&clearTimeout(this._timer);this._animateStartTime=+new Date;this._animateStartAngle=this._angle;this._animate()},_animate:function(){var a=+new Date,b=a-this._animateStartTime>this._parameters.duration;if(b&&!this._parameters.animatedGif)clearTimeout(this._timer);else{if(this._canvas||this._vimage||this._img)a=this._parameters.easing(0,a-this._animateStartTime,this._animateStartAngle,this._parameters.animateTo-this._animateStartAngle,this._parameters.duration),
this._rotate(~~(10*a)/10);this._parameters.step&&this._parameters.step(this._angle);var c=this;this._timer=setTimeout(function(){c._animate.call(c)},10)}this._parameters.callback&&b&&(this._angle=this._parameters.animateTo,this._rotate(this._angle),this._parameters.callback.call(this._rootObj))},_rotate:function(){var a=Math.PI/180;return IE?function(a){this._angle=a;this._container.style.rotation=a%360+"deg";this._vimage.style.top=-(this._rotationCenterY-this._imgHeight/2)+"px";this._vimage.style.left=
-(this._rotationCenterX-this._imgWidth/2)+"px";this._container.style.top=this._rotationCenterY-this._imgHeight/2+"px";this._container.style.left=this._rotationCenterX-this._imgWidth/2+"px"}:d?function(a){this._angle=a;this._img.style[d]="rotate("+a%360+"deg)";this._img.style[f]=this._parameters.center.join(" ")}:function(b){this._angle=b;b=b%360*a;this._canvas.width=this._width;this._canvas.height=this._height;this._cnv.translate(this._imgWidth*this._aspectW,this._imgHeight*this._aspectH);this._cnv.translate(this._rotationCenterX,
this._rotationCenterY);this._cnv.rotate(b);this._cnv.translate(-this._rotationCenterX,-this._rotationCenterY);this._cnv.scale(this._aspectW,this._aspectH);this._cnv.drawImage(this._img,0,0)}}()};IE&&(Wilq32.PhotoEffect.prototype.createVMLNode=function(){document.createStyleSheet().addRule(".rvml","behavior:url(#default#VML)");try{return!document.namespaces.rvml&&document.namespaces.add("rvml","urn:schemas-microsoft-com:vml"),function(a){return document.createElement("<rvml:"+a+' class="rvml">')}}catch(a){return function(a){return document.createElement("<"+
a+' xmlns="urn:schemas-microsoft.com:vml" class="rvml">')}}}())})(jQuery);


(function ($) {
  Drupal.TBMetroz = Drupal.TBMetroz || {};
  Drupal.TBMetroz.backToTopButton = null;
  Drupal.TBMetroz.supportedScreens = [0.5, 479.5, 719.5, 959.5, 1049.5];
  Drupal.TBMetroz.PopupIsVisible = false;
  Drupal.TBMetroz.isFresh = true;

  Drupal.TBMetroz.btnToTop = function(){
    var current_top = $(document).scrollTop();
    if(current_top !=0) {
      Drupal.TBMetroz.backToTopButton.fadeIn(300);
    }
    else {
      Drupal.TBMetroz.backToTopButton.fadeOut(300);
    }
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    Drupal.TBMetroz.backToTopButton.css({position: 'fixed', bottom: 0, top: 'auto', right: '20px'});
    /*
    var h3 = current_top + $(window).height() - Drupal.TBMetroz.backToTopButton.height() - 20;
        Drupal.TBMetroz.backToTopButton.css({top: h3});
    */

  }

  Drupal.TBMetroz.initViewAsGirdOrList = function(){
    var primaryTabs = $('#block-views-modes-modes #view-modes');
    if(primaryTabs.length > 0){
      var filerBar = $('.page-taxonomy .views-exposed-form .views-exposed-widgets');
      filerBar.find('.views-widget-view-as').remove();
      var $dom = $('<div class="views-exposed-widget views-widget-view-as"></div>');
      primaryTabs.find('li').filter(function(index){
        if(index < 2){
          var $this = $(this);
          var link = $this.find('a').clone();
          link.removeClass("active");
          (index % 2 == 0) ? link.addClass('grid-view ') : link.addClass('list-view');
          $dom.append(link);
          $this.hide();
        }
      });
      //primaryTabs.hide();

      filerBar.prepend($dom);
    }
  }

  Drupal.TBMetroz.initUserLoginBlockFields = function(){
    var userBlock = $('#account-wrapper .block-user');
    if(userBlock.length > 0){
      userBlock.find('.form-item-name input').placeholder({value:Drupal.t('Name')});
      userBlock.find('.form-item-pass input').passwordPlaceHolder({value:Drupal.t('Password')});
      userBlock.find('.form-item-pass').after(userBlock.find('.form-actions'));
    }
  }

  Drupal.TBMetroz.initSearchFormBlock = function(){
    $('.search-form .form-text, .form-item-search-block-form .form-text').placeholder({value: 'Search site here...'});
  }

  Drupal.TBMetroz.replacePagerText = function(){
    $('.pager-first a').text(Drupal.t('First'));
    $('.pager-previous a').text(Drupal.t('Prev'));
    $('.pager-next a').text(Drupal.t('Next'));
    $('.pager-last a').text(Drupal.t('Last'));
  }

  Drupal.TBMetroz.hideMenuPopup = function(target){
    target.slideUp(400); // PREV: 300
    Drupal.TBMetroz.PopupIsVisible = false;
  }

  Drupal.TBMetroz.showMenuPopup = function(target){
    target.slideDown(400); // PREV: 300
    Drupal.TBMetroz.PopupIsVisible = true;
  }
/*
  Drupal.TBMetroz.initSpecialMenu = function(){
    $('#special-menu li').each(function(){
      var $this = $(this);
      $this.bind('mouseover click', function(e){
        $(this).siblings().each(function(){
          var sibTarget = $($(this).data('data'));
         // if(sibTarget.css('display') != 'none') sibTarget.slideUp(0);
        });
        var target = $($(this).data('data'));
        if(target.css('display') == 'none') {
          //show pop-up content
          target.css({position: 'absolute', right: 0});
          var arrow = target.find('.arrow');
          var parent = $this.parents('#header');
          var arrowRight = (parent.offset().left + parent.width()) - ($this.offset().left + Math.round($this.width()/2)) - (arrow.width()/2);
          arrow.css({right: arrowRight});
          target.slideDown(400); //PREV: 300
          if(navigator.userAgent.indexOf('Windows Phone') == -1){
            target.bind('mouseenter', function(){
              Drupal.TBMetroz.PopupIsVisible = true;
            });
            target.bind('mouseleave', function(){
              Drupal.TBMetroz.hideMenuPopup(target);
            });
            $this.bind('mouseover', function(){
              Drupal.TBMetroz.PopupIsVisible = true;
            }).bind('mouseleave', function(){
              Drupal.TBMetroz.PopupIsVisible = false;
              var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
              if(windowWidth > Drupal.TBResponsive.supportedScreens[3]){
                setTimeout(function(){
                  if(!Drupal.TBMetroz.PopupIsVisible) Drupal.TBMetroz.hideMenuPopup(target);
                }, 400); //PREV: 300
              }
            });
          }

        }
          else Drupal.TBMetroz.hideMenuPopup(target);
        });
        if($this.attr('data')){
          $this.data('data',$this.attr('data')).removeAttr('data');
        }
      });
  }
*/
  Drupal.TBMetroz.initStickyBar = function(){
    var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
    if(windowWidth < Drupal.TBResponsive.supportedScreens[4]) return;
    var current_top = $(document).scrollTop();
    var header = $('#header-wrapper');
    if(current_top ==0) {
      header.removeClass('sticky-bar');
    }else{
      header.addClass('sticky-bar');
    }
  }

  Drupal.behaviors.actionTBMetroz = {
    attach: function (context) {
      if(!Drupal.TBMetroz.isFresh) return;// Fix repeat binding issues for a product display with multiple product references.

      Drupal.TBMetroz.backToTopButton = $('.btn-btt');
      Drupal.TBMetroz.backToTopButton.smoothScroll(); // bind smooth scroll for back to top button
      Drupal.TBMetroz.btnToTop();
      Drupal.TBMetroz.initStickyBar();
      Drupal.TBMetroz.initViewAsGirdOrList();
      Drupal.TBMetroz.initUserLoginBlockFields();
      Drupal.TBMetroz.initSearchFormBlock();
      Drupal.TBMetroz.replacePagerText();
      //Drupal.TBMetroz.initSpecialMenu();
      $(window).scroll(function(){
        Drupal.TBMetroz.btnToTop();
        Drupal.TBMetroz.initStickyBar();
      });

      Drupal.TBMetroz.isFresh = false;
    }
  };

  //pulldown hide/show
  Drupal.behaviors.headerPullDown = {
    attach: function (context, settings) {

///      (function($) {
        
        //Open pulldown as default if checkend via backend
//    		$(document).ready(function() {
//    			var isOpen = $('#node-41').find('.field-name-field-mantieni-aperto .field-item').text();
//    			if(isOpen == 1) {
//    				$('a#header-pull').trigger('click');
//    			}
//    		});

        $(document).ready(function() {
            var isOpen = $(document.getElementById('#node-41')).find('.field-name-field-mantieni-aperto .field-item').text();
                if(isOpen == 1) {
                    $(document.getElementById('a#header-pull')).trigger('click');
                }
        });


//        $(document.getElementById('a#header-pull')).on('click', function(e) {
          $('a#header-pull').click(function(e) {
            e.preventDefault();
            var animateDeg,
            	animatePull;

            if($(this).find('span').hasClass('rotated')) {
	            animatePull = '-=389px';  
              animateDeg = 0;
            }
            else {
              animatePull = '+=389px';
              animateDeg = -180;   
            }

            $(this).find('span').toggleClass('rotated').rotate({
                animateTo: animateDeg,
                duration:800
            });
            
            $('#pulldown-wrapper .container').slideToggle(800, 'easeOutExpo');
            $('#page').animate({ 'padding-top': animatePull }, 800, 'easeOutExpo');
            
        });

    		//always close pulldown area when scrolling
//        $(window).on('scroll', function(e) {
          $(window).scroll( function(e) {
            $('#pulldown-wrapper .container').slideUp(800, 'easeOutExpo');
            var animateDeg;
            if($('a#header-pull').find('span').hasClass('rotated')) {
            	$('a#header-pull').find('span').toggleClass('rotated').rotate({
	                animateTo: 0,
	                duration:800
	            });
	             $('#page').animate({ 'padding-top': '-=389px' }, 800, 'easeOutExpo');
            }
        });
///      })(jQuery);		

    }
  };


  //apply pulldown style
  Drupal.behaviors.headerPullDownStyle = {
    attach: function (context, settings) {
      
///      (function($) { 
        
        var styleBlock =  $('#node-41'),
        textColor = styleBlock.find('.field-name-field-colore-del-testo .field-item').text(),
        linkColor = styleBlock.find('.field-name-field-colore-dei-link .field-item').text(),
        bgColor = styleBlock.find('.field-name-field-color-di-sfondo .field-item').text(),
        titleColor = styleBlock.find('.field-name-field-colore-titoli .field-item').text(),
        bgLink = styleBlock.find('.field-name-field-colore-sfondo-link .field-item').text();

        //bg & text
        $('#header-wrapper #pulldown-wrapper, #header-wrapper #pulldown-wrapper .container ').css({ 'background': '#' + bgColor, 'color': '#' + textColor });
  
        //links
        $('#header-wrapper #pulldown-wrapper .container ').find('a').css({ color: '#' + linkColor });
        //$('#header-wrapper #pulldown-wrapper .field-type-link-field').find('a').css({ 'background': '#' + bgLink });
        
        //titles' color
        $('#header-wrapper #pulldown-wrapper .field-name-title ').find('h2').css({ color: '#' + titleColor });
        

///      })(jQuery);
      
    }
  };

  //Show grand total
  Drupal.behaviors.headerTotalCart = {
    attach: function (context, settings) {
        
///        (function($) { 
        
          $('.view-id-commerce_cart_block').bind("DOMSubtreeModified",function(){
  
            var totalCart = $(this).find('.line-item-total-raw').text();
            $('#special-menu .special-menu-total').empty().text(totalCart);

          });
          var totalCart = $('#block-views-shopping-cart-block').find('.line-item-total-raw').text();
          $('#special-menu .special-menu-total').empty().text(totalCart);
        
///        })(jQuery);
    
    }
  };

  Drupal.behaviors.cartSlideUpDown = {
    attach: function (context, settings) {
      $('#block-dc-ajax-add-cart-ajax-shopping-cart-teaser')
      .mouseenter(function() {
        clearTimeout($(this).data('timeoutId'));
        $('#block-dc-ajax-add-cart-ajax-shopping-cart').slideDown();
      }).mouseleave(function() {
        var timeoutId = setTimeout(function(){
          $('#block-dc-ajax-add-cart-ajax-shopping-cart').slideUp();
          }, 550);
        $('#block-dc-ajax-add-cart-ajax-shopping-cart')
        .mouseenter(function() {
          clearTimeout($('#block-dc-ajax-add-cart-ajax-shopping-cart-teaser').data('timeoutId'));
          }).mouseleave(function() {
            $('#block-dc-ajax-add-cart-ajax-shopping-cart').slideUp();
        });
        $(this).data('timeoutId', timeoutId);
      });
    }
  };

  //PoopOvers
  Drupal.behaviors.poopOver = {
    attach: function (context, settings) {

      
///      (function($) { 
      
        var rightButtons = $("#search-tool, #info-tool, #fav-tool");
      
    		rightButtons
    		.mouseenter(function(){
    		    clearTimeout($(this).data('timeoutId'));
    		    //rightButtons.find('.special-menu-content').fadeOut(100);   
    		    $(this).find('.special-menu-content').fadeIn(300);
    		}).mouseleave(function(){
    		    var someElement = $(this),
    		        timeoutId = setTimeout(function(){
    		            someElement.find('.special-menu-content').fadeOut(100);
    		        }, 650);
    		    //set the timeoutId, allowing us to clear this trigger if the mouse comes back over
    		    someElement.data('timeoutId', timeoutId); 
    		});


// BEGIN PSEUDOFIXED POPOVER

//POPOVER LEFT
         $('.region.region-panel-prefooter-1 .block-content')
         .mouseenter(function() {
             clearTimeout($(this).data('timeoutId'));
             $('#popover-left-wrapper').fadeIn(200);
         }).mouseleave(function() {
             var timeoutId = setTimeout(function(){
                     $('#popover-left-wrapper').fadeOut(100);
                 }, 550);
             $('#popover-left-wrapper')
             .mouseenter(function() {
                 clearTimeout($('.region.region-panel-prefooter-1 .block-content').data('timeoutId'));
             }).mouseleave(function() {
                 $('#popover-left-wrapper').fadeOut(100);
             });
             $(this).data('timeoutId', timeoutId);
          });


//POPOVER RIGHT
         $('.region.region-panel-prefooter-2 .block-content')
         .mouseenter(function() {
             clearTimeout($(this).data('timeoutId'));
             $('#popover-right-wrapper').fadeIn(200);
         }).mouseleave(function() {
             var timeoutId = setTimeout(function(){
                     $('#popover-right-wrapper').fadeOut(100);
                 }, 550);
             $('#popover-right-wrapper')
             .mouseenter(function() {
                 clearTimeout($('.region.region-panel-prefooter-2 .block-content').data('timeoutId'));
             }).mouseleave(function() {
                 $('#popover-right-wrapper').fadeOut(100);
             });
             $(this).data('timeoutId', timeoutId);
          });

// END PSEUDOFIXED POPOVER

    		
//  		//FIX ME: it's a dirty method but it works for now. We save the element we want so show when triggering events below
//  		$('.region.region-panel-prefooter-1').data('popoverElement', '#popover-left-wrapper');
//  		$('.region.region-panel-prefooter-2').data('popoverElement', '#popover-right-wrapper');
//  		
//  		var footerPanels = $('.region.region-panel-prefooter-2, .region.region-panel-prefooter-1');
//  		
//  		//-- popover footer informations
//  		footerPanels
//  		.mouseenter(function() {
//  			var offset = $(this).offset();
//  			var posY = offset.top;
//  			var posX = offset.left;
//  			var someElement = $( $(this).data('popoverElement') );
//  			
//  			//close all panels
//  			footerPanels.each(function() {
//    			$( $(this).data('popoverElement') ).fadeOut(300); //PREV: 100
//  			});
//   			
//    			//clearing timeout
//    			clearTimeout($(this).data('timeoutId'));
//    			
//    			someElement.css({ 'top': posY+'px', 'left': posX+'px' }).fadeIn(300);
//    
//    		})
//        .mouseleave(function() {
//    			var someElement = $($(this).data('popoverElement'));
//    		  var timeoutId = setTimeout(function(){
//    		            someElement.fadeOut(300); //PREV: 100
//    		        }, 650);
//    		    //set the timeoutId, allowing us to clear this trigger if the mouse comes back over
//    		    $(this).data('timeoutId', timeoutId); 
//    		});
    		
///      })(jQuery);

    }
  };
  
  //Show the details of an order*/
  Drupal.behaviors.showOrder = {
    attach: function (context, settings) {
    
///      (function($) { 
      
    		$(".arrow-close" ).click(function(e) {
    			if($(this).attr("alt") == "close"){
    				$(this).attr("src", "/sites/all/themes/tb_metroz/images/arrow-3.png" );
    				$(this).parents("tr.odd").next("tr.even").show();
    				$(this).attr("alt", "open");
    			}else{
    				$(this).attr("src", "/sites/all/themes/tb_metroz/images/arrow.png" );
    				$(this).parents("tr.odd").next("tr.even").hide();
    				$(this).attr("alt", "close");
    			}
    		});
    		
///      })(jQuery);
    
    }
  };
  
  Drupal.behaviors.plusMinusButtons = {
  	attach: function (context, settings) {
  		
///  		(function($) {
  		
//  			$(document).on('click', '.minus , .plus', function(e){
                     $('.minus, .plus').click( function(e){ 
  				e.stopImmediatePropagation();
  				fieldName = $(this).attr('data-field');
  				
  				var input = $("input[name='"+fieldName+"']");
  				var currentVal = parseInt(input.val());
  				
  				if (!isNaN(currentVal)) {
  					if($(this).hasClass('minus')) {
  						if( currentVal > 1 ) {
  							input.val(currentVal - 1).change();
  						}else return;
  					} else if($(this).hasClass('plus')) {							
  							input.val(currentVal + 1).change();
  					}
  				} else {
  					input.val(1);
  				}
  			});
  			
///  		})(jQuery);	
  		
  	}
  };
  
  Drupal.behaviors.menuWidth = {
  	attach: function (context, settings) {
  		(function megaMenuFull($) {
  			//main menu
  			//$('#menu-bar-wrapper li.mega').on('mouseover mouseout', function(event) {
  			$('#menu-bar-wrapper li.mega').bind('mouseover mouseout', function(event) {
    				// Get window width and dropdown's position
  				var windowWidth = window.innerWidth ? window.innerWidth : $(window).width();
  				var megaMenu = $(this).find('.mega-dropdown-menu');
  				var menuPosition = megaMenu.parent().offset();
          
  				if ( megaMenu.length > 0 ) {
  					// change width and margin according to the data retrieved 
  					megaMenu.css({ width: windowWidth + 'px', marginLeft: '-' + menuPosition.left + 'px' });
  					megaMenu.find('.mega-dropdown-inner').first().addClass('container');
  				}
  			});
  		})(jQuery);	
  	}
  };

  Drupal.behaviors.fixUnwantedAjax = {
    attach: function (context, settings) {
      
      (function fixUnwantedAjax($) {
      
        //clean all events attached to the login form's submit button
        $('#user-login-form').find('.form-submit').unbind();
                
      })(jQuery); 
      
    }
  };

//  Drupal.behaviors.horribleFix= {
//      attach: function (context, settings) {
//          $('#menu-bar-wrapper li.mega').on('mouseover mouseout', function(event) {});
//      }
//  };
  
})(jQuery);
